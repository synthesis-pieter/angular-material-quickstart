# angular-material-quickstart #

This repo is intended to get you started with an Angular application quickly.

## Getting started ##

Start mock backend server:
```bash
$ cd mock
$ npm start
```

Start Angular frontend:
```bash
$ npm install
$ npm start
```

![](.readme\startup.gif)
