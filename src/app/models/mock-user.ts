export class MockUser {
  id: string = "";
  name: string = "";
  surname: string = "";
  age: number = 0;
}
