import {Component, OnInit} from '@angular/core';
import {MockUserService} from '../services/mock-user.service';
import {MockUser} from "../models/mock-user";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  mockUsers: MockUserViewModel[] = [];

  constructor(private mockUserService: MockUserService) {

  }

  ngOnInit(): void {
    debugger
    this.mockUserService.getUsers()
      .subscribe((users: MockUser[]) => {
        this.mockUsers = this.mapMockUserModelsToViewModels(users);
      })
  }

  /**
   * Maps an array of MockUser to MockUserViewModel
   *
   * @param models
   * @returns {MockUserViewModel[]}
   */
  mapMockUserModelsToViewModels(models: MockUser[]): MockUserViewModel[] {
    return models.map(this.mapMockUserToViewModel)
  }


  /**
   * Maps a single MockUser Model to a ViewModel.
   * @param MockUser
   * @returns {MockUserViewModel}
   */
  mapMockUserToViewModel(model: MockUser): MockUserViewModel {
    return {
      name: model.name,
      surname: model.surname,
      age: model.age,
      id: model.id
    }
  }
}

class MockUserViewModel {
  id: string = "";
  name: string = "";
  surname: string = "";
  age: number = 0;
}
