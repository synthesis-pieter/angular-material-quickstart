import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from "rxjs/operators";
import {MockUser} from "../models/mock-user";

@Injectable({
  providedIn: 'root'
})
export class MockUserService {
  constructor(private http: HttpClient) {

  }

  /**
   * Returns mock users from the backend
   *
   * @returns {Observable<MockUser[]>}
   */
  getUsers(): Observable<MockUser[]> {
    return this.http.get<MockUserDTO[]>(`/api/user/all`)
      .pipe(map(dto => this.mapMockUsersDTOtoModel(dto)))
  }

  /**
   * Maps array of MockUserDTO to array ofd MockUser
   *
   * @param usersDTO
   * @returns {MockUser[]}
   */
  mapMockUsersDTOtoModel(usersDTO: MockUserDTO[]): MockUser[] {
    return usersDTO.map(this.mapMockUserDTOtoModel);
  }

  /**
   * Maps MockUserDTO to MockUser
   *
   * @param dto
   * @returns {{surname: string, name: string, id: string, age: number}}
   */
  mapMockUserDTOtoModel(dto: MockUserDTO): MockUser {
    return {
      id: dto.id,
      name: dto.name,
      surname: dto.surname,
      age: dto.age
    }
  }
}

export class MockUserDTO {
  id: string = "";
  name: string = "";
  surname: string = "";
  age: number = 0;
}



