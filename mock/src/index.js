const express = require("express");
const faker = require('faker');
const app = express();
const port = 8080; // default port to listen

class MockUser {
  constructor(id, name, surname, age) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.age = age;
  }
}

// define a route handler for the default home page
app.get("/", (req, res) => {
  res.send("Hello from Express!");
});

/**
 * Generates 10 random users using Faker.js and returns them in an array
 */
app.get("/api/user/all", (req, res) => {
  const randomUsers = []

  for (let i = 0; i < 10; i++) {
    const user = new MockUser(faker.datatype.uuid(), faker.name.firstName(), faker.name.lastName(), Math.floor(Math.random() * 80) + 1);
    randomUsers.push(user);
  }

  res.json(randomUsers);
});

// start the Express server
app.listen(port, '0.0.0.0', () => {
  console.log(`server started at http://localhost:${port}`);
});
